import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const dialogDelete = ref(false);
  let idC = 0;
  const product = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProduct();
      product.value = res.data;
      console.log(res);
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }
  const saveProduct = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      // clearProduct();
      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  };

  const editProduct = (product: Product) => {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  };

  const deleteProduct = async () => {
    loadingStore.isLoading = true;

    try {
      const res = await productService.deleteProduct(idC);
      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
    dialogDelete.value = false;
  };

  const deletePD = (id: number) => {
    dialogDelete.value = true;
    idC = id;
  };

  return {
    product,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    dialogDelete,
    deletePD,
  };
});
