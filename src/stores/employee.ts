import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
export const useEmployeeStore = defineStore("employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const dialogDelete = ref(false);
  let idC = 0;
  const employee = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({
    name: "",
    username: "",
    password: "",
    birthday: "",
    address: "",
    tel: "",
    email: "",
    other_contact: "",
    date_start: "",
    role: "",
    rate: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = {
        name: "",
        username: "",
        password: "",
        birthday: "",
        address: "",
        tel: "",
        email: "",
        other_contact: "",
        date_start: "",
        role: "",
        rate: 0,
      };
    }
  });
  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployee();
      employee.value = res.data;
      console.log(res);
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }
  const saveEmployee = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }
      dialog.value = false;
      await getEmployees();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  };

  const editEmployee = (employee: Employee) => {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  };

  const deleteEmployee = async () => {
    loadingStore.isLoading = true;

    try {
      const res = await employeeService.deleteEmployee(idC);
      await getEmployees();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
    dialogDelete.value = false;
  };

  const deleteEP = (id: number) => {
    dialogDelete.value = true;
    idC = id;
  };

  return {
    employee,
    getEmployees,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    deleteEmployee,
    dialogDelete,
    deleteEP,
  };
});
