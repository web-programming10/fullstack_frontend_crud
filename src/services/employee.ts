import http from "@/services/axios";
import type Employee from "@/types/Employee";
function getEmployee() {
  return http.get("/employees");
}
function saveEmployee(employee: Employee) {
  return http.post("/employees", employee);
}
function updateEmployee(id: number, employee: Employee) {
  return http.patch("/employees/" + id, employee);
}
function deleteEmployee(id: number) {
  return http.delete("/employees/" + id);
}
export default { getEmployee, saveEmployee, updateEmployee, deleteEmployee };
