export default interface Employee {
  id?: number;
  name: string;
  username: string;
  password: string;
  birthday: string;
  address: string;
  tel: string;
  email: string;
  other_contact?: string;
  date_start: string;
  role: string;
  rate: number;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
